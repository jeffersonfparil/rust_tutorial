/////////////////////////////////////////////////////////////////////////////////////////
// # Unused variables and functions, and trailing underscore to supress compiler warnings
////////////////////////////////////////////////////////////////////////////////////////
fn use_all_defined_variables_unless_stated_otherwise() {
	// Defining or initialising variables without a trailing underscore promises a use of it;
	// however `#[warn(unused_variables)]` is turned on by default.
	let unused_variables_will_cause_a_warning = 10;
	let _unused_variables_with_trailing_underscore_will_not_cause_a_warning = 5;
}

fn unused_functions_will_cause_a_warning() {
	// The same goes for defined functions.
	println!("This function is initialised but won't be called.");
}

fn _unused_function_with_trailing_underscore_will_not_cause_a_warning(){
        // The same goes for defined functions.
        println!("This function is initialised but won't be called.");
}

////////////////////////
// # Variable mutability
////////////////////////
fn variable_mutability() {
	let immutable = 10;
	let mut mutable = 3.14;
	println!("Immutable variable = {}", immutable);
	println!("Mutable variable = {}", mutable);
	mutable = mutable + 2.023;
	println!("See, the mutable variable is now = {}", mutable);
}

////////////////////////////
// # Main execution function
////////////////////////////
fn main() {
	use_all_defined_variables_unless_stated_otherwise();
	variable_mutability();
}
