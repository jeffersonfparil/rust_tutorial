# Learning Rust using the Book

- The [book](https://doc.rust-lang.org/book/)
- Install rust: `curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh`
- Compilation: `rustc filename.rs --out-dir target/output/directory`
